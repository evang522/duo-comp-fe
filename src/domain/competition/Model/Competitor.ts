import {CompetitorStructure} from "../Types/CompetitorStructure";

export default class Competitor {
    public constructor(
        private _competitionPoints: number,
        private _duolingoId: string,
        private _duolingoUsername: string,
        private _profilePhotoUrl: string | null,
        private _currentLanguage: string,
        private _streak: number,
    ) {
    }

    public static fromObjectLiteral(competitor: CompetitorStructure): Competitor {
        return new Competitor(
            competitor.competitionPoints,
            competitor.duolingoUserId,
            competitor.duolingoUserName,
            competitor.profilePhotoUrl,
            competitor.currentLanguage,
            competitor.streak,
        );
    }

    public streak(): number
    {
        return this._streak;
    }

    public currentLanguage(): string {
        return this._currentLanguage;
    }

    public competitionPoints(): number {
        return this._competitionPoints;
    }

    public duolingoId(): string {
        return this._duolingoId;
    }

    public duolingoUsername(): string {
        return this._duolingoUsername;
    }

    public profilePhotoUrl(): string | null {
        return this._profilePhotoUrl;
    }
}
