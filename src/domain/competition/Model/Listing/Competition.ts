import { CompetitionStructure } from '../../Types/Listing/CompetitionStructure';

export default class Competition {
    public constructor(
        private _id: string,
        private _competitorCount: number,
        private _name: string,
        private _startDate: Date,
        private _endDate: Date,
    ) {

    }

    public id(): string {
        return this._id;
    }

    public competitorCount(): number {
        return this._competitorCount;
    }

    public name(): string {
        return this._name;
    }

    public startDate(): Date {
        return this._startDate;
    }

    public endDate(): Date {
        return this._endDate;
    }

    public static fromStruct(competition: CompetitionStructure): Competition
    {
        return new this(
            competition.id,
            competition.competitorCount,
            competition.name,
            new Date(competition.startDate),
            new Date(competition.endDate)
        );
    }
}
