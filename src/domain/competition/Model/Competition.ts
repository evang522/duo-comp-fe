import Competitor from "./Competitor";
import {CompetitionStructure} from "../Types/CompetitionStructure";
import {TimeInterval} from "time-interval-js";
import {PointsMapStructure} from "../Types/PointsMapStructure";

export default class Competition
{

    public constructor(
        private _id: string,
        private _name: string,
        private _startDate: Date,
        private _endDate: Date,
        private _competitors: Competitor[],
        private _winner: string | null,
        private _description: string | null,
    )
    {
    }

    public static fromObjectLiteral(competition: CompetitionStructure): Competition
    {
        return new this(
            competition.id,
            competition.name,
            new Date(competition.startDate),
            new Date(competition.endDate),
            competition.competitors.map((competitor) => Competitor.fromObjectLiteral(competitor)),
            competition.winner,
            competition.description
        );
    }

    public description(): string | null
    {
        return this._description;
    }

    public name(): string
    {
        return this._name;
    }

    public id(): string
    {
        return this._id;
    }

    public startDate(): Date
    {
        return this._startDate;
    }

    public endDate(): Date
    {
        return this._endDate;
    }

    public winner(): Competitor | null
    {
        if (!this._winner)
        {
            return null;
        }

        return this.competitors().find((competitor) => competitor.duolingoId() === this._winner) || null;
    }

    public competitors(): Competitor[]
    {
        return this._competitors;
    }

    public startsInTheFuture(): boolean
    {
        return this.startDate().getTime() - Date.now() > 0;
    }

    public endedInThePast(): boolean
    {
        return Date.now() - this.endDate().getTime() > 0;
    }

    public timeUntilEnd(): TimeInterval
    {
        return TimeInterval.fromTimeBetweenTwoDates(this.endDate(), Date.now());
    }

    public timeUntilStart(): TimeInterval
    {
        return TimeInterval.fromTimeBetweenTwoDates(this.startDate(), Date.now());
    }

    public isCurrent(): boolean
    {
        const now = Date.now();

        return now > this.startDate().getTime() && now < this.endDate().getTime();
    }

    public leaderDuoUsername(): string | null
    {
        const leader = this.competitors()
            .sort((a: Competitor, b: Competitor) => b.competitionPoints() - a.competitionPoints())[0];

        if (!leader)
        {
            return null;
        }

        return leader.duolingoId();
    }

    public getPointsMap(): PointsMapStructure
    {
        const pointsMap: PointsMapStructure = {};

        this.competitors().forEach((competitor: Competitor) =>
        {
            pointsMap[competitor.duolingoId()] = competitor.competitionPoints();
        })

        return pointsMap;
    }

    public findUserByDuolingoId(duolingoId: string): Competitor | null
    {
        return this.competitors().find((competitor: Competitor) => competitor.duolingoId() === duolingoId) || null;
    }

}
