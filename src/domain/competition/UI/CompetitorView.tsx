import Competitor from '../Model/Competitor';
import { Card, Image } from 'react-bootstrap';
import { ReactElement } from 'react';
import ISO639 from 'iso-639-1';
import Flag from 'react-world-flags';

interface Props
{
    competitor: Competitor
    isLeader: boolean
    pointGainsSinceLastLoad?: number;
}

export default function CompetitorView(props: Props): ReactElement
{
    return (
        <Card
            bg="dark"
            border={props.isLeader ? 'danger' : undefined}
            className="m-1 shadow competitor-card"
            style={{width: '18rem'}}
        >
            <Card.Body style={{margin: '0 auto'}}>
                <Image
                    style={{cursor: 'pointer'}}
                    onClick={() => window.open(`https://www.duolingo.com/profile/${props.competitor.duolingoUsername()}`)}
                    roundedCircle src={`${props.competitor.profilePhotoUrl()!}/large`}/>


                <Card.Title className="badge-warning rounded m-2 p-1">
                    {props.competitor.duolingoUsername()}
                </Card.Title>
                <Card.Text className="lead" style={{color: 'white'}}>
                    {props.competitor.competitionPoints()} points
                </Card.Text>
                <div className="container" style={{fontSize: '1.1rem'}}>
                    <Card.Text style={{minWidth: '9rem'}} className="badge badge-success p-2 pt-2  m-1">
                        Learning: {ISO639.getName(props.competitor.currentLanguage())}  <Flag code={(resolveFlagCode(props.competitor.currentLanguage()))} style={{width: '25px', borderRadius: '3px'}} />
                    </Card.Text>
                    <Card.Text className="badge badge-primary p-2 m-1">
                        Streak: {props.competitor.streak()}
                    </Card.Text>
                    <Card.Text className="badge badge-danger p-2 m-1">
                        Gains: {props.pointGainsSinceLastLoad ?? 0}
                    </Card.Text>
                </div>

            </Card.Body>
        </Card>
    );
}

function resolveFlagCode(code: string): string {
     console.log(code);
    switch(code) {
        case 'zh':
            return 'CN';
        case 'la':
            return 'VA';
        default:
            return code;
    }
}

