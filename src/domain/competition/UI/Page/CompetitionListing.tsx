import React, { Component, ReactElement } from 'react';
import CompetitionClient from '../../Service/CompetitionClient';
import Competition from '../../Model/Listing/Competition';
import { Button, Card } from 'react-bootstrap';

export default class CompetitionListing extends Component<any, { competitions: Competition[] }> {
    public async componentDidMount(): Promise<void> {
        const competitionClient = new CompetitionClient();
        const competitions = await competitionClient.getPublicListing();
        this.setState({
            competitions,
        })
    }

    public state = {
        competitions: []
    }

    public render() {
        return (
            <>
                <header style={{
                    position: 'fixed',
                    background: '#444444',
                    boxShadow: '2px .5rem .5rem rgba(0,0,0,.15)',
                    width: '100%',
                    top: '0',
                    minHeight: '65px',
                    zIndex: 1200,
                    fontWeight: 'bolder',
                    color: 'white',
                }}>
                    <h3 className="mt-3 text-center">Public Competitions</h3>
                </header>
                <br/>
                <br/>
                <br/>
                <br/>
                <div className="container">
                    <div className="row justify-content-center">
                        {this.state.competitions.map((competition, index) => this.renderCompetition(competition, index))}
                    </div>
                </div>
            </>
        );
    }

    private renderCompetition(competition: Competition, index: number): ReactElement {
        return (
            <Card key={index} bg="dark" style={{width: '20rem', margin: '1rem', boxShadow: '3px 3px 8px #BBBBBB', padding: '10px'}}>
                <Card.Title style={{color: 'white'}} className="text-center">{competition.name()}</Card.Title>
                <Card.Text>
                    <small style={{color: 'white'}}>Competitors: {competition.competitorCount()}</small>
                </Card.Text>
                <Button variant="primary" href={`/competition/${competition.id()}`}>View</Button>
            </Card>
        );
    }
}
