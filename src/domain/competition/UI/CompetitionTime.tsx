import Competition from "../Model/Competition";
import {useState} from "react";
import CalculateTimeString from "../Service/CalculateTimeString";

interface Props {
    competition: Competition;
}

export default function CompetitionTime(props: Props) {
    const calculateTimeString = new CalculateTimeString();
    const [timeString, setTimeString] = useState<string>(calculateTimeString.get(props.competition));

    setInterval(() => {
        setTimeString(calculateTimeString.get(props.competition));
    }, 1000)

    return <span style={{color: 'white'}}>{timeString}</span>;
}
