import {Button, Modal} from "react-bootstrap";
import parse from "html-react-parser";
import React from "react";

interface Props {
    description: string | null;
    setShowDescription: (show: boolean) => void;
    showDescription: boolean;
}

export default function DescriptionView(props: Props) {
    const {description, setShowDescription, showDescription} = props;
    return (
        <>
            {description && (
                <Button
                    className="mb-4"
                    variant="outline-dark"
                    onClick={() => setShowDescription(!showDescription)}>
                    Description
                </Button>
            )}

            <Modal style={{top: '10%'}} className="pr-0 mt-5" onHide={() => setShowDescription(false)} show={showDescription}>
                <div className="container">
                    <div className="col-12 p-3">
                        {description && parse(description!)}
                    </div>
                </div>
            </Modal>
        </>

    );
}
