import {PointsMapStructure} from "../../Types/PointsMapStructure";
import Competition from "../../Model/Competition";

export default class LastPointsClient
{
    public static STORAGE_KEY_GAINS = 'last_points_for_competition:';

    public constructor(
        private localStorageClient: Storage = window.localStorage,
    )
    {
    }

    public getForCompetition(competitionId: string): PointsMapStructure | null
    {
        const map = this.localStorageClient.getItem(LastPointsClient.STORAGE_KEY_GAINS + competitionId);

        if (!map)
        {
            return null;
        }

        return JSON.parse(map);
    }

    public setForCompetition(competition: Competition): void
    {
        const stringifiedGainsMap = JSON.stringify(competition.getPointsMap());

        localStorage.setItem(LastPointsClient.STORAGE_KEY_GAINS + competition.id(), stringifiedGainsMap);
    }

}
