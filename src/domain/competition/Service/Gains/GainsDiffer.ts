import LastPointsClient from "./LastPointsClient";
import {GainsDiffStructure} from "../../Types/GainsDiffStructure";
import Competition from "../../Model/Competition";

export default class GainsDiffer
{
    public constructor(
        private lastPointsClient: LastPointsClient = new LastPointsClient()
    )
    {
    }

    public updateAndDiffCompetition(competition: Competition): GainsDiffStructure | null
    {
        const lastPointsRecord = this.lastPointsClient.getForCompetition(competition.id());
        this.lastPointsClient.setForCompetition(competition);

        if (!lastPointsRecord)
        {
            return null;
        }

        const latestPointsMap = competition.getPointsMap();

        const gainsDiffMap: GainsDiffStructure = {};

        for (const competitorId in latestPointsMap)
        {
            if (!latestPointsMap.hasOwnProperty(competitorId))
            {
                continue;
            }
            gainsDiffMap[competitorId] = latestPointsMap[competitorId] - lastPointsRecord[competitorId];
        }

        return gainsDiffMap;
    }
}
