import NotifierInterface from "../../../../infrastructure/Service/Notifier/NotifierInterface";
import ToastNotifier from "../../../../infrastructure/Service/Notifier/ToastNotifier";
import Competition from "../../Model/Competition";
import {GainsDiffStructure} from "../../Types/GainsDiffStructure";

export default class GainsNotifier
{
    public constructor(
        private notifier: NotifierInterface = new ToastNotifier()
    )
    {
    }

    public writeNotificationsForGainsDiffMap(competition: Competition, gainsDiffMap: GainsDiffStructure): void
    {
        for (const userId in gainsDiffMap)
        {
            if (!gainsDiffMap.hasOwnProperty(userId) || !gainsDiffMap[userId])
            {
                continue;
            }

            const userName = competition.findUserByDuolingoId(userId)!.duolingoUsername();
            this.notifier.notify(`${userName} gained ${gainsDiffMap[userId]} points since you last visited!`);
        }
    }

}
