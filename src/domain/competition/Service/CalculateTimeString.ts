import Competition from "../Model/Competition";

export default class CalculateTimeString {
    public get(competition: Competition): string {

        if (competition.endedInThePast()) {
            return this.forPastCompetition(competition);
        }

        if (competition.isCurrent()) {
            return this.forCurrentCompetition(competition);
        }

        return this.forFutureCompetition(competition);
    }

    private forFutureCompetition(competition: Competition): string {
        if (competition.timeUntilStart().inDays() >= 1) {
            return `Competition starts in ${competition.timeUntilStart().inDays().toFixed(0)} days.`
        } else {
            return `Competition starts in ${competition.timeUntilStart().inHours().toFixed(1)} hours.`
        }    }

    private forCurrentCompetition(competition: Competition): string {
        if (competition.timeUntilEnd().inDays() >= 1) {
            return `Competition ends in ${competition.timeUntilEnd().inDays().toFixed(0)} days.`
        } else {
            return `Competition ends in ${competition.timeUntilEnd().inHours().toFixed(1)} hours.`
        }
    }

    private forPastCompetition(competition: Competition): string {
        if (competition.timeUntilEnd().inDays() >= 1) {
            return `Competition ended ${competition.timeUntilEnd().inDays().toFixed(0)} days ago.`
        } else {
            return `Competition ended ${competition.timeUntilEnd().inHours().toFixed(1)} hours ago.`
        }
    }
}
