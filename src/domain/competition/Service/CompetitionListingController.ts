import Competition from "../Model/Competition";
import CompetitionClient from "./CompetitionClient";

export default class CompetitionListingController {
    public constructor(
        private competitionClient = new CompetitionClient(),
    ) {

    }

    public async handlePageLoad(
        competitionId: string,
        successCallback: (competition: Competition) => void,
        errorCallback: (e: Error) => void
    ): Promise<void> {
        try {
            const competition = await this.competitionClient.getCompetition(competitionId);
            successCallback(competition);
        } catch (e) {
            errorCallback(e);
        }
    }

}
