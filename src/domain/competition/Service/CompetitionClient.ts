import { parameters } from '../../../application/_config/parameters/parameters';
import Competition from '../Model/Competition';
import CompetitionFromListing from '../Model/Listing/Competition';
import { CompetitionStructure as CompetitionFromListingStructure } from '../Types/Listing/CompetitionStructure';
import { CompetitionStructure } from '../Types/CompetitionStructure';

export default class CompetitionClient {
    public async getCompetition(competitionId: string) {
        const response = await fetch(parameters.apiBaseUrl + '/api/competition/' + competitionId);
        if (response.status > 299) {
            throw new Error(response.statusText);
        }

        const competitionStructure: CompetitionStructure = await response.json();

        return Competition.fromObjectLiteral(competitionStructure);
    }

    public async getPublicListing(): Promise<CompetitionFromListing[]> {
        const response = await fetch(parameters.apiBaseUrl + '/api/competition');
        if (response.status > 299) {
            throw new Error(response.statusText);
        }

        const competitions: CompetitionFromListingStructure[] = await response.json();
        return competitions.map((competition) => CompetitionFromListing.fromStruct(competition))
    }
}
