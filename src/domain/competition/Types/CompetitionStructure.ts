import {CompetitorStructure} from "./CompetitorStructure";

export interface CompetitionStructure {
    id: string;
    startDate: string;
    endDate: string;
    name: string;
    competitors: CompetitorStructure[];
    winner: string | null;
    description: string|null;
}
