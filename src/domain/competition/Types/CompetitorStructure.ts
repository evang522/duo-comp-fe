export interface CompetitorStructure
{
    competitionPoints: number;
    duolingoUserId: string;
    duolingoUserName: string;
    profilePhotoUrl: string;
    currentLanguage: string;
    streak: number;
}
