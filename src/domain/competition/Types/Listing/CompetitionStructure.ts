export interface CompetitionStructure {
    name: string;
    id: string;
    startDate: string;
    endDate: string;
    competitorCount: number,
}
