import {Spinner} from "react-bootstrap";

export default function FullPageSpinner() {
    return (
        <div style={{
            position: 'fixed',
            background: 'rgba(255,255,255,0.4)',
            width: '100%',
            height: '100%',
            zIndex: 1000,
        }}>
            <Spinner
                as="span"
                animation="border"
                role="status"
                aria-hidden={"true"}
                className="mt-4"
                style={{
                    position: 'fixed',
                    top: '30%',
                    zIndex: 1100,
                    left: 'calc(50% - 16px)',
                }}
            />
        </div>

    )
}

