import NotifierInterface from "./NotifierInterface";
import {toast} from "react-toastify";

export default class ToastNotifier implements NotifierInterface
{
    public notify(message: string): void
    {
        toast(message, {
            autoClose: 5000,
            className: 'toast-class-name',
            position: "bottom-right",
            hideProgressBar: true,
        });
    }
}
