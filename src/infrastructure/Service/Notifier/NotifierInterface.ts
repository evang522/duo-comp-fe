export default interface NotifierInterface
{
    notify(message: string): void;
}
