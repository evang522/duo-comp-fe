import React, {useEffect, useState} from 'react';

import Competition from "./domain/competition/Model/Competition";
import Competitor from "./domain/competition/Model/Competitor";
import CompetitionTime from "./domain/competition/UI/CompetitionTime";
import CompetitorView from "./domain/competition/UI/CompetitorView";
import CompetitionListingController from "./domain/competition/Service/CompetitionListingController";
import {TimeInterval} from "time-interval-js";
import {Button} from "react-bootstrap";
import DescriptionView from "./domain/competition/UI/DescriptionView";
import {RouteComponentProps} from "react-router";
import FullPageSpinner from "./ui/atom/FullPageSpinner";
import GainsDiffer from "./domain/competition/Service/Gains/GainsDiffer";
import {GainsDiffStructure} from "./domain/competition/Types/GainsDiffStructure";
import GainsNotifier from "./domain/competition/Service/Gains/GainsNotifier";
import {toast, ToastContainer} from "react-toastify";

interface Props extends RouteComponentProps<{ competitionId: string }>
{
}

function CompetitionView(props: Props)
{
    const listingPageController = new CompetitionListingController();
    const gainsDiffer = new GainsDiffer();
    const gainsNotifier = new GainsNotifier();

    const [loading, setLoading] = useState<boolean>(false);
    const [competition, setCompetition] = useState<Competition | null>(null);
    const [error, setError] = useState<string | null>(null);
    const [showDescription, setShowDescription] = useState(false);
    const {competitionId} = props.match.params;
    const [gainsMap, setGains] = useState<GainsDiffStructure | null>(null);


    useEffect(() =>
    {
        toast.configure({
            limit: 3
        });

        if (!competitionId)
        {
            setError('No Competition Id provided');
            return;
        }

        if (error || competition || loading)
        {
            return;
        }

        setLoading(true);
        listingPageController.handlePageLoad(
            competitionId,
            (competition: Competition) =>
            {
                setCompetition(competition);
                const newGainsMap = gainsDiffer.updateAndDiffCompetition(competition);
                setGains(newGainsMap)
                newGainsMap && gainsNotifier.writeNotificationsForGainsDiffMap(competition, newGainsMap);
                setLoading(false);
            },
            () =>
            {
                setError('There was an issue loading the competition.')
                setLoading(false);
            }
        )

        setInterval(() =>
        {
            setLoading(true);
            listingPageController.handlePageLoad(
                competitionId,
                (competition: Competition) =>
                {
                    setCompetition(competition);
                    const newGainsMap = gainsDiffer.updateAndDiffCompetition(competition);
                    setGains(newGainsMap)
                    newGainsMap && gainsNotifier.writeNotificationsForGainsDiffMap(competition, newGainsMap);
                    setLoading(false);
                },
                () => setLoading(false)
            )
        }, TimeInterval.forSpecifiedMinutes(9).inMilliseconds())

    }, [
        competition,
        competitionId,
        error,
        listingPageController,
        showDescription,
        gainsDiffer,
        gainsMap,
        gainsNotifier
    ])

    return (
        <div className="mb-5">

            <div className="text-center">
                <ToastContainer/>
                {error && <h2 className="p-2 text-center alert-warning">{error}</h2>}
                {error && <img className="m-5" alt="Duolingo owl in crutches" src="/duolingo_owl_error.png"/>}
                {loading && <FullPageSpinner/>}
                {competition && (
                    <>

                        <header style={{
                            position: 'fixed',
                            background: '#444444',
                            boxShadow: '2px .5rem .5rem rgba(0,0,0,.15)',
                            width: '100%',
                            top: '0',
                            minHeight: '65px',
                            zIndex: 1200,
                            fontWeight: 'bolder',
                            color: 'white'
                        }}>
                            <h3 className="mt-3">{competition.name()}</h3>
                        </header>
                        <div style={{
                            marginTop: '4.5rem'
                        }}>


                            {competition.winner() &&
                            (
                                <div>
                                    <div className="badge-success badge p-3 m-3">
                                        {competition.winner()!.duolingoUsername()} won
                                        with {competition.winner()?.competitionPoints()} points!
                                    </div>
                                </div>
                            )
                            }
                            <DescriptionView
                                setShowDescription={setShowDescription}
                                showDescription={showDescription}
                                description={competition.description()}
                            />
                            <br/>
                            <br/>
                            <CompetitionTime competition={competition}/>
                        </div>

                        <Button className="m-3" variant="outline-secondary" onClick={() =>
                        {
                            setLoading(true);

                            listingPageController.handlePageLoad(
                                competitionId!,
                                (competition: Competition) =>
                                {
                                    setCompetition(competition);
                                    const newGainsMap = gainsDiffer.updateAndDiffCompetition(competition);
                                    setGains(newGainsMap)
                                    newGainsMap && gainsNotifier.writeNotificationsForGainsDiffMap(competition, newGainsMap);
                                    setLoading(false);
                                },
                                () =>
                                {
                                    setError('There was an issue loading the competition.');
                                    setLoading(false);
                                }
                            )

                        }}>Refresh data
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                 className="bi bi-arrow-counterclockwise ml-1" viewBox="0 0 16 16">
                                <path
                                    d="M8 3a5 5 0 1 1-4.546 2.914.5.5 0 0 0-.908-.417A6 6 0 1 0 8 2v1z"
                                />
                                <path
                                    d="M8 4.466V.534a.25.25 0 0 0-.41-.192L5.23 2.308a.25.25 0 0 0 0 .384l2.36 1.966A.25.25 0 0 0 8 4.466z"
                                />
                            </svg>
                        </Button>
                        <br/>
                        <div className="container">
                            <div className="row justify-content-center">
                                {competition.competitors()
                                    .sort((a, b) => b.competitionPoints() - a.competitionPoints())
                                    .map((competitor: Competitor, index: number) => (
                                        <CompetitorView
                                            pointGainsSinceLastLoad={gainsMap ? gainsMap[competitor.duolingoId()] : undefined}
                                            isLeader={competition?.leaderDuoUsername() === competitor.duolingoId()}
                                            competitor={competitor} key={index}/>
                                    ))}
                            </div>

                        </div>
                    </>
                )}

            </div>
        </div>
    );
}

export default CompetitionView;
