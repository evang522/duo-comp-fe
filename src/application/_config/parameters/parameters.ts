import AppConfig from "../../Model/AppConfig";
import {devParams} from "./dev";
import {prodParams} from "./prod";

const paramsToInject = AppConfig.fromEnvironment().inEnv('dev') ? devParams : prodParams;

export const parameters = {
    ...paramsToInject
}
