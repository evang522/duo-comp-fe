export default function RouteNotFound() {
    return (
        <div className="container">
            <div className="text-center mt-5">
                <h1 className="font-weight-bolder">404 - Page not Found!</h1>
                <img className="m-5" alt="Duolingo owl in crutches" src="/duolingo_owl_error.png"/>
            </div>
        </div>
    );
}
