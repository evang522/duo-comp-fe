import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import CompetitionView from '../../CompetitionView';
import 'react-toastify/dist/ReactToastify.min.css';
import '../../App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import React from 'react';
import RouteNotFound from './RouteNotFound';
import CompetitionListing from '../../domain/competition/UI/Page/CompetitionListing';


export default function Root() {
    return (
        <Router>
            <Switch>
                <Route path="/competition/:competitionId" component={CompetitionView}/>
                <Route path="/competitions" component={CompetitionListing}/>
                <Route path="*" component={RouteNotFound}/>
            </Switch>
        </Router>
    );
}
