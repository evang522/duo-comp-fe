export default class AppConfig {

    public constructor(
        private env: string,
    ) {}

    public static fromEnvironment() {
        return new this(process.env.REACT_APP_ENV!);
    }

    public inEnv(env: ENVVAR): boolean
    {
        return this.env === env;
    }
}

export type ENVVAR = 'prod' | 'dev';
